import java.util.Scanner;

public class Calculadora {
    static int suma(int a, int b){
        return a + b;        	
    }
    static int resta(int a, int b){
        return a - b;
    }
    static int multiplicacion(int a, int b){
        return a * b;
    }
    static int division(int a, int b){
        return a / b;
    }
    public static void main(String[] args) { 
        Scanner capt = new Scanner(System.in);
        int opcion = 1; 
        int numero1; 
        int numero2;
        while (opcion != 5){
            System.out.println("Inserte numero 1: ");
            numero1 = capt.nextInt();
            System.out.println("Inserte numero 2: ");
            numero2 = capt.nextInt();
            System.out.println("Eliga operación: \n 1.Suma\n 2.Resta\n 3.Multiplicacion\n 4.Division\n 5.Salir");
            opcion = capt.nextInt();
            if (opcion == 1){
                System.out.println("La suma es: " +  suma(numero1, numero2));
            }
            if (opcion == 2){
                System.out.println("La resta es: " + resta(numero1, numero2));
            }
            if (opcion == 3){
                System.out.println("La multiplicacion es: " + multiplicacion(numero1, numero2));
            }
            if (opcion == 4){
                System.out.println("La division es: " + division(numero1, numero2));
            }
        }
    }
}
